#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "resultat.h"
#include <QIntValidator>
#include <QApplication>
#include <QTranslator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Calculateur d'interest"));

    ui->solde->setValidator(new QIntValidator(this));
    ui->depot->setValidator(new QIntValidator(this));
    ui->anne->setValidator(new QIntValidator(1,99,this));
    ui->interest->setValidator(new QIntValidator(1,9,this));
    ui->aug_depot->setValidator(new QIntValidator(this));
    ui->chq_annee->setValidator(new QIntValidator(1,10,this));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_calcul_clicked()
{
    if (ui->solde->text().isEmpty())
    {
            QMessageBox::critical(this, tr("Erreur"), tr("Veuillez entrer le montant de depart de votre compte de banque"));
            return; // Arret de la methode
    }
    if (ui->depot->text().isEmpty())
    {
            QMessageBox::critical(this, tr("Erreur"), tr("Veuillez entrer le montant de depot récurant a chaque mois"));
            return; // Arret de la methode
    }
    if (ui->anne->text().isEmpty())
    {
            QMessageBox::critical(this, tr("Erreur"), tr("Veuillez entrer le nombre d'années "));
            return; // Arret de la methode
    }
    if (ui->interest->text().isEmpty())
    {
            QMessageBox::critical(this, tr("Erreur"), tr("Veuillez entrer le taux d' intérêt "));
            return; // Arret de la methode
    }

    //QlineEdit to Qstring
    Solde = ui->solde->text();
    Depot = ui->depot->text();
    Anne = ui->anne->text();
    Interest = ui->interest->text();
    Aug_depot = ui->aug_depot->text();
    Chq_annee = ui->chq_annee->text();

    //Qstring to int
    int SOLDE  = Solde.toInt();
    int DEPOT  = Depot.toInt();
    int ANNE  = Anne.toInt();
    int INTEREST  = Interest.toInt();
    int AUG_DEPOT = Aug_depot.toInt();
    int CHQ_ANNEE = Chq_annee.toInt();
    int ans=0;
    int profit=0;
    int DEPOT_total=0;
    QString out,out2;

    for (int i=1; i<= ANNE ; i++){

        for( int j=0; j<12;j++){
            ans= (SOLDE * INTEREST)/1200;
            SOLDE =SOLDE +ans + DEPOT;
            profit += ans;
            DEPOT_total += DEPOT;
        }
        profit_list << profit;
        annee_list << i;
        solde_list << SOLDE;
        depot_list << DEPOT_total;
        DEPOT_total=0;
        QString number = QString::number(i);
        QString solde_string = QString::number(SOLDE);
        QString profit_string = QString::number(profit);
        out += "Année " + number + " : " + solde_string + "\n\n";
        out2 += "Année " + number + " : " + profit_string + "\n\n";
        if (ui->aug_depot->text().isEmpty()){}
        else{
            if (i % CHQ_ANNEE == 0){
                DEPOT += AUG_DEPOT;
            }
        }

    }


    resultat *total =new resultat(out,out2,profit_list,solde_list,depot_list,annee_list,this);
    total->exec();

}


/*
void MainWindow::on_anglais_clicked()
{



}


void MainWindow::on_francais_clicked()
{

}
*/
