#ifndef GRAPH_H
#define GRAPH_H

#include <QDialog>
#include <QtCharts>
#include <mainwindow.h>


namespace Ui {
class graph;
}

class graph : public QDialog
{
    Q_OBJECT

public:
    explicit graph(QList<int> &profit_list,QList<int> &solde_list,QList<int> &depot_list,QList<int> &annee_list ,QWidget *parent);
    ~graph();

private:
    Ui::graph *ui;
    QChart *chart;
    QChartView *chartView;
    QList<int> profit_list ,solde_list ,depot_list,annee_list;
};

#endif // GRAPH_H
