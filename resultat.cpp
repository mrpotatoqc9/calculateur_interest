#include "resultat.h"
#include "ui_resultat.h"
#include "mainwindow.h"
#include <graph.h>


resultat::resultat(QString &data , QString &data2 , QList<int> &profit_list ,QList<int> &solde_list ,QList<int> &depot_list ,QList<int> &annee_list , QWidget *parent) :
    QDialog(parent),
    ui(new Ui::resultat)
{
    ui->setupUi(this);
     setWindowTitle(tr("Resultat"));


     ui->DATA->setPlainText(data);
     ui->DATA->setReadOnly(true);


     ui->DATA2->setPlainText(data2);
     ui->DATA2->setReadOnly(true);

     profit = profit_list;
     solde = solde_list;
     depot = depot_list;
     annee =annee_list;

}

resultat::~resultat()
{
    delete ui;
}

void resultat::on_pushButton_clicked()
{
    this->close();
}

void resultat::on_graph_clicked()
{
    graph *graphique = new graph(profit,solde,depot,annee,this);

    graphique->resize(1600, 800);


    graphique->exec();
}
