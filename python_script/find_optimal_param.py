from tqdm import tqdm

class investement:
	def __init__(self, startBalance, monthlyDeposit, yearlyGrowth):
		self.balance = startBalance
		self.deposit = monthlyDeposit
		self.growth = yearlyGrowth
	
	def updateDeposit(self, newMonthlyDeposit):
		self.deposit = newMonthlyDeposit
	
	def computeBlanceFor(self, nbYears):
		for i in range(1, nbYears+1):
			for i in range(1, 13):
				self.balance += (self.growth/12)* self.balance
				self.balance += self.deposit



invest = investement(7000, 175, 5/100)

for i in tqdm(range(36)):
	invest.computeBlanceFor(1)
	invest.updateDeposit(invest.deposit + invest.deposit*(5/100))
	

print(invest.balance)


"""
target = 750000
salaryStart = 1066*26
maxSavingPerYear = 20/100 * salaryStart
print(maxSavingPerYear, maxSavingPerYear/26, maxSavingPerYear/52)

ageStart = 30
agePensionTime = 67
deltaAge = agePensionTime - ageStart

print(target / deltaAge, maxSavingPerYear)

"""
