#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QMessageBox>
//#include <QLabel>
#include<QPushButton>
#include <QTranslator>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


protected:
    //void changeEvent(QEvent*);

private slots:

    void on_calcul_clicked();



protected slots:
    //void on_anglais_clicked();
    //void on_francais_clicked();

private:
    Ui::MainWindow *ui;

    QLineEdit *solde;
    QLineEdit *interest;
    QLineEdit *depot;
    QLineEdit *anne;
    QLineEdit *aug_depot;
    QLineEdit *chq_annee;
    QString Anne, Solde , Depot , Interest , Aug_depot , Chq_annee ;

    QPushButton *anglais;

    QTranslator m_translator; // contains the translations for this application
    QTranslator m_translatorQt; // contains the translations for qt
    QString m_currLang; // contains the currently loaded language
    QList<int> profit_list ,solde_list ,depot_list,annee_list;

};
#endif // MAINWINDOW_H
