#!/bin/bash

sudo apt install -y build-essential cmake qt5-default libqt5charts5-dev libqt5charts5
mkdir build && cd build
cmake ..
make
