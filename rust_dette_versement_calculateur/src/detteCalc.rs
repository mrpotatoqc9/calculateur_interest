use std::env;
fn main() {
	let args: Vec<String> = env::args().collect();
	if args.len() < 4{
		panic!("missing arguments:\n example : dette interet nb_annees \n");
	}

	let mut dette = 100.0 * &args[1].parse::<f64>().unwrap_or(0.0);
	let interet = &args[2].parse::<f64>().unwrap_or(0.0);
	let m_interet = (interet/12.0) / 100.0;
	let nb_annees = &args[3].parse::<f64>().unwrap_or(0.0);
	let nb_mois = nb_annees * 12.0;
	println!("calcul paiment pour : {}$ à {}% pendant {}an", dette , interet , nb_annees);
	
	let numerateur = m_interet * (1f64+m_interet).powf(nb_mois);
	let denumerateur = (1f64+m_interet).powf(nb_mois) - 1.0;
	let versement = (numerateur/denumerateur) * dette;
	println!("versement = {} par mois", versement/100.0);
	
	let mut total_interet : f64 = 0.0;
	
	for i in 0..nb_mois as i64{
		total_interet = total_interet + dette * m_interet;
		dette = dette + dette * m_interet;
		dette = dette - versement;
		if dette < versement{
			println!("paiement fini {} mois plus tot que prevu", nb_mois as i64 - (i+1));
			break;
		}
	}
	println!("total interet payer = {}", total_interet/100.0);
	println!("reste {} à payer", dette/100.0);
	
}
