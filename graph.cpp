#include "graph.h"
#include "ui_graph.h"
#include "mainwindow.h"
//#include <QDebug>

graph::graph(QList<int> &profit_list ,QList<int> &solde_list,QList<int> &depot_list,QList<int> &annee_list ,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::graph)
{
    ui->setupUi(this);
    setWindowTitle(tr("graphique"));
    QBarSet *set0 = new QBarSet(tr("Solde"));
    QBarSet *set1 = new QBarSet(tr("Dépot"));
    QBarSet *set2 = new QBarSet(tr("Profit engendré par les intérêts"));
    QStringList categories;
    for(int i=0;i<annee_list.size();i++)
    {
        *set0<<solde_list[i];
        *set1<<depot_list[i];
        *set2<<profit_list[i];
        categories<<QString::number(annee_list[i]);
    }


    QBarSeries *series = new QBarSeries();
    series->append(set0);
    series->append(set1);
    series->append(set2);


    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle(tr("Graphique du calculateur d'intérêt"));
    chart->setAnimationOptions(QChart::SeriesAnimations);




    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    int max = solde_list[annee_list.size()/2];
    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0,max);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);


    ui->gridLayout->addWidget(chartView,0,0);
}

graph::~graph()
{
    delete ui;
}
