<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="50"/>
        <source>Solde du compte</source>
        <translation>Account balance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <source>Dépot faites par mois</source>
        <translation>Deposit made each month</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="64"/>
        <source> Taux des intérêts du compte   </source>
        <translation>Interest rate</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="71"/>
        <source>À chaque :</source>
        <translation>Every</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>Nombre d&apos;année a calculer</source>
        <translation>number of years to be calculated</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>Ans</source>
        <translation>Years</translation>
    </message>
    <message>
        <source>solde du compte</source>
        <translation type="vanished">Bank account balance</translation>
    </message>
    <message>
        <source>dépot faites par mois</source>
        <translation type="vanished">Deposit made each month</translation>
    </message>
    <message>
        <source> intérêt   </source>
        <translation type="vanished">Interest</translation>
    </message>
    <message>
        <source>A chaque :</source>
        <translation type="vanished">Every</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>Augmentez les dépots de :</source>
        <translation>Deposit increase</translation>
    </message>
    <message>
        <source>nombre d&apos;année</source>
        <translation type="vanished">For how many years</translation>
    </message>
    <message>
        <source>ans</source>
        <translation type="vanished">year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="134"/>
        <source>Calcul</source>
        <translation>Calculation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="13"/>
        <source>Calculateur d&apos;interest</source>
        <translation>Interest calculator</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <location filename="mainwindow.cpp" line="38"/>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="48"/>
        <source>Erreur</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <source>Veuillez entrer le montant de depart de votre compte de banque</source>
        <translation>Please enter a valid amount for the balance of your bank account</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="38"/>
        <source>Veuillez entrer le montant de depot récurant a chaque mois</source>
        <translation>Please enter a deposit amount</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <source>Veuillez entrer le nombre d&apos;années </source>
        <translation>Please enter a the number of years to be calculated</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>Veuillez entrer le taux d&apos; intérêt </source>
        <translation>Please enter the interest rate of the bank account</translation>
    </message>
</context>
<context>
    <name>graph</name>
    <message>
        <location filename="graph.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="graph.cpp" line="11"/>
        <source>graphique</source>
        <translation>Chart</translation>
    </message>
    <message>
        <location filename="graph.cpp" line="12"/>
        <source>Solde</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="graph.cpp" line="13"/>
        <source>Dépot</source>
        <translation>Deposit total</translation>
    </message>
    <message>
        <location filename="graph.cpp" line="14"/>
        <source>Profit engendré par les intérêts</source>
        <translation>Profit</translation>
    </message>
    <message>
        <location filename="graph.cpp" line="33"/>
        <source>Graphique du calculateur d&apos;intérêt</source>
        <translation>Interest calculator chart</translation>
    </message>
</context>
<context>
    <name>resultat</name>
    <message>
        <location filename="resultat.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="resultat.ui" line="44"/>
        <source>quittter</source>
        <translation>quit</translation>
    </message>
    <message>
        <location filename="resultat.ui" line="83"/>
        <source>Solde du compte de banque</source>
        <translation>Bank account balance</translation>
    </message>
    <message>
        <location filename="resultat.ui" line="90"/>
        <source>Profits des intérêts</source>
        <translation>Interest Profit</translation>
    </message>
    <message>
        <location filename="resultat.ui" line="22"/>
        <source>Graphique</source>
        <translation>Chart</translation>
    </message>
    <message>
        <location filename="resultat.cpp" line="12"/>
        <source>Resultat</source>
        <translation>Result</translation>
    </message>
</context>
</TS>
