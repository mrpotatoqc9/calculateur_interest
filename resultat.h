#ifndef RESULTAT_H
#define RESULTAT_H

#include <QDialog>
#include <QTextEdit>
#include <QMessageBox>


namespace Ui {
class resultat;
}

class resultat : public QDialog
{
    Q_OBJECT

public:
    explicit resultat(QString &data , QString &data2 , QList<int> &profit_list ,QList<int> &solde_list ,QList<int> &depot_list ,QList<int> &annee_list , QWidget *parent);
    ~resultat();

private slots:
    void on_pushButton_clicked();

    void on_graph_clicked();

private:
    Ui::resultat *ui;
    QTextEdit *DATA;
    QTextEdit *DATA2;
    QList<int> profit , solde, depot,annee;
};

#endif // RESULTAT_H
